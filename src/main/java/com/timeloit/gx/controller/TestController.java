package com.timeloit.gx.controller;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TimeZone;

/**
 * 测试控制器
 *
 * @author HouKunLin
 * @date 2020/2/19 0019 15:21
 */
@Slf4j
@Data
@RestController
@RequestMapping
public class TestController {
    private ApplicationContext applicationContext;

    public TestController(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @RequestMapping("/test")
    public Object indexTest() {
        TimeZone timeZone = TimeZone.getDefault();
        HashMap<String, Object> map = new LinkedHashMap<>(16);
        map.put("ID", timeZone.getID());
        map.put("displayName", timeZone.getDisplayName());
        map.put("zoneId", timeZone.toZoneId());
        map.put("DSTSavings", timeZone.getDSTSavings());
        map.put("useDaylightTime", timeZone.useDaylightTime());
        map.put("observesDaylightTime", timeZone.observesDaylightTime());
        map.put("applicationId", applicationContext.getId());
        map.put("applicationName", applicationContext.getApplicationName());
        map.put("applicationDisplayName", applicationContext.getDisplayName());
        map.put("applicationBeanDefinitionCount", applicationContext.getBeanDefinitionCount());
        map.put("timestamp", new Date());
        return map;
    }
}
