package com.timeloit.gx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDockerDeployApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDockerDeployApplication.class, args);
    }

}
