FROM java:8-alpine
ENV TZ Asia/Shanghai
WORKDIR /app
ADD target/spring-boot-docker-deploy-0.0.1-SNAPSHOT.jar ./
EXPOSE 8080
ENTRYPOINT ["java","-jar","spring-boot-docker-deploy-0.0.1-SNAPSHOT.jar"]